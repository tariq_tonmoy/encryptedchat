﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace EChatClient
{
    public class ClientSessionKeyGenerator
    {
        public byte[] ClientPublicKey { get; set; }
        ECDiffieHellmanCng clientECD = new ECDiffieHellmanCng();

        public byte[] ClientKey { get; set; }

        public byte[] GeneratePublicKey()
        {

            clientECD.KeyDerivationFunction = ECDiffieHellmanKeyDerivationFunction.Hash;
            clientECD.HashAlgorithm = CngAlgorithm.Sha256;
            ClientPublicKey = clientECD.PublicKey.ToByteArray();
            return ClientPublicKey;
        }

        public void GenerateSessionKey(byte[] ServerPublicKey)
        {
            CngKey k = CngKey.Import(ServerPublicKey, CngKeyBlobFormat.EccPublicBlob);
            ClientKey = clientECD.DeriveKeyMaterial(CngKey.Import(ServerPublicKey, CngKeyBlobFormat.EccPublicBlob));

        }
    }
}
