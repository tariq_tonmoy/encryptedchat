﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace EChatClient
{
    public class SymmetricCryptographicTool
    {
        public byte[] iv = null;
        public byte[] key = null;
        public byte[] encryptedMessage = null;
        public string Message { get; set; }
        public void EncryptAES(byte[] key, string secretMessage)
        {
            using (Aes aes = new AesCryptoServiceProvider())
            {
                aes.Key = key;
                iv = aes.IV;

                // Encrypt the message
                using (MemoryStream ciphertext = new MemoryStream())
                using (CryptoStream cs = new CryptoStream(ciphertext, aes.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    byte[] plaintextMessage = Encoding.UTF8.GetBytes(secretMessage);
                    cs.Write(plaintextMessage, 0, plaintextMessage.Length);
                    cs.FlushFinalBlock();
                    cs.Close();
                    encryptedMessage = ciphertext.ToArray();
                }
            }
        }

        public string DecryptAES(byte[] key, byte[] encryptedMessage, byte[] iv)
        {

            using (Aes aes = new AesCryptoServiceProvider())
            {
                aes.Key = key;
                aes.IV = iv;
                using (MemoryStream plaintext = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(plaintext, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(encryptedMessage, 0, encryptedMessage.Length);
                        cs.Close();
                        Message = Encoding.UTF8.GetString(plaintext.ToArray());
                        return Message;
                    }
                }
            }
        }

        public void GenerateSymmetricKey()
        {
            RijndaelManaged rm = new RijndaelManaged();
            this.key = rm.Key;
            this.iv = rm.IV;
        }

    }
}
