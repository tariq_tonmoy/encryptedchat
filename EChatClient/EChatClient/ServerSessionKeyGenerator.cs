﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace EChatClient
{
    public class ServerSessionKeyGenerator
    {
        public byte[] serverPublicKey { get; set; }
        ECDiffieHellmanCng serverECD = new ECDiffieHellmanCng();
        public byte[] serverKey { get; set; }

        public byte[] GeneratePublicKey()
        {

            serverECD.KeyDerivationFunction = ECDiffieHellmanKeyDerivationFunction.Hash;
            serverECD.HashAlgorithm = CngAlgorithm.Sha256;
            serverPublicKey = serverECD.PublicKey.ToByteArray();
            return serverPublicKey;
        }
        public void GenerateSessionKey(byte[] ClientPublicKey)
        {

            CngKey k = CngKey.Import(ClientPublicKey, CngKeyBlobFormat.EccPublicBlob);
            serverKey = serverECD.DeriveKeyMaterial(CngKey.Import(ClientPublicKey, CngKeyBlobFormat.EccPublicBlob));

        }
    }
}
