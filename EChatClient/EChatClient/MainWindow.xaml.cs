﻿using EChatCommon;
using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EChatClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IHubProxy proxy = null;
        HubConnection hubConnection = null;
        static List<User> LiveUsers = new List<User>();
        static Dictionary<UserGroup, User> JoinedGroups = new Dictionary<UserGroup, User>();
        static List<Message> Messages = new List<Message>();
        static User me = new User();
        static UserGroup currentGroup = new UserGroup();
        static List<CheckBox> checkBoxes = new List<CheckBox>();
        static User EncUser = new User();
        static SymmetricCryptographicTool sct = new SymmetricCryptographicTool();
        static ClientSessionKeyGenerator cskg = new ClientSessionKeyGenerator();
        static byte[] SessionKey;
        RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
        RSAParameters RSAKeyInfo = new RSAParameters();
        public MainWindow()
        {
            InitializeComponent();

            this.OnMessageReceived += MainWindow_OnMessageReceived;
            this.OnStatusMessageReceived += MainWindow_OnStatusMessageReceived;
            this.listViewLiveUsers.ItemsSource = LiveUsers;
            RSAKeyInfo = RSA.ExportParameters(false);

        }


        void MainWindow_OnStatusMessageReceived(Status status, object[] items)
        {

            if (status == Status.SignIn)
            {
                JObject jo = JObject.FromObject(items[0]);
                User user = jo.ToObject<User>();



                if (user.Username != me.Username)
                {
                    LiveUsers.Add(user);
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {

                        textBoxReceivedMessage.Text += "System: " + user.Username + " is Online" + addDateTime();



                    }));
                }
                else
                {
                    me.UserID = user.UserID;
                }

            }
            else if (status == Status.SignOut)
            {
                JObject jo = JObject.FromObject(items[0]);
                User user = jo.ToObject<User>();

                LiveUsers.Remove(LiveUsers.FirstOrDefault(x => x.Username == user.Username));
                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    textBoxReceivedMessage.Text += "System: " + user.Username + " is Offline" + addDateTime();

                }));
            }
            else if (status == Status.Error)
            {


                string errStr = (string)items[0];

                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    textBoxReceivedMessage.Text += "Error: " + errStr + addDateTime();
                }));
            }
            else if (status == Status.GroupCreate)
            {
                JObject jo = JObject.FromObject(items[0]);

                UserGroup userGroup = jo.ToObject<UserGroup>();

                JoinedGroups.Add(userGroup, me);
                var v = LiveUsers.FirstOrDefault(x => x.Username == me.Username);
                if (v != null)
                    v.GroupList.Add(userGroup.GroupID);

                currentGroup = userGroup;
                SetGroupSpecificConfig();
            }
            else if (status == Status.GroupJoin)
            {
                JObject jo = JObject.FromObject(items[0]);
                User creator = jo.ToObject<User>();



                jo = JObject.FromObject(items[1]);
                User user = jo.ToObject<User>();

                jo = JObject.FromObject(items[2]);
                UserGroup group = jo.ToObject<UserGroup>();


                JArray array = (JArray)items[3];
                List<User> usrList = array.ToObject<List<User>>();




                LiveUsers.FirstOrDefault(x => x.Username == user.Username).GroupList.Add(group.GroupID);

                JoinedGroups.Add(group, LiveUsers.FirstOrDefault(x => x.Username == creator.Username));

                if (me.Username != creator.Username)
                {
                    if (!LiveUsers.FirstOrDefault(x => x.Username == creator.Username).GroupList.Exists(y => y == group.GroupID))
                    {
                        LiveUsers.FirstOrDefault(x => x.Username == creator.Username).GroupList.Add(group.GroupID);
                    }
                    this.Dispatcher.BeginInvoke(new Action(() =>
                                       {

                                           textBoxReceivedMessage.Text += user.Username + " Joined the Group\n\n";
                                           Messages.Add(new Message() { UserMessage = user.Username + " Joined the Group\n\n", MessageID = Guid.NewGuid(), Sender = new User() { Username = "System" }, TargetGroup = group, Timestamp = DateTime.Now });
                                       }));
                }
                currentGroup = group;
                foreach (var u in usrList)
                {
                    if (!LiveUsers.FirstOrDefault(x => x.Username == u.Username).GroupList.Exists(y => y == group.GroupID))
                    {

                        LiveUsers.FirstOrDefault(x => x.Username == u.Username).GroupList.Add(group.GroupID);
                    }
                }

                SetGroupSpecificConfig();

            }
            else if (status == Status.GroupLeave)
            {


                JObject jo = JObject.FromObject(items[0]);
                User creator = jo.ToObject<User>();

                jo = JObject.FromObject(items[1]);
                User user = jo.ToObject<User>();

                jo = JObject.FromObject(items[2]);
                UserGroup group = jo.ToObject<UserGroup>();

                LiveUsers.FirstOrDefault(x => x.Username == user.Username).GroupList.Remove(group.GroupID);
                if (currentGroup.GroupID == group.GroupID)
                {
                    currentGroup = new UserGroup();
                    currentGroup.GroupName = "Select/Create a Group";
                }

                if (me.Username == user.Username)
                {
                    JoinedGroups.Remove(JoinedGroups.FirstOrDefault(x => x.Key.GroupID == group.GroupID).Key);
                }
                else
                {
                    var u = LiveUsers.FirstOrDefault(x => x.Username == user.Username);
                    u.GroupList.Remove(group.GroupID);

                }
                SetGroupSpecificConfig();
            }

        }


        void SetGroupSpecificConfig()
        {
            this.Dispatcher.BeginInvoke(new Action(() =>
            {

                checkBoxes.ForEach(x => x.IsChecked = false);
                textBoxReceivedMessage.Text = "";
                comboBoxGroup.Items.Clear();
                foreach (var v in LiveUsers)
                {
                    if (v.GroupList.Exists(y => y == currentGroup.GroupID) && checkBoxes.Count > 0)
                    {
                        checkBoxes.FirstOrDefault(x => x.Tag.ToString() == v.Username).IsChecked = true;
                    }
                }


                foreach (var m in Messages)
                {
                    if (m.TargetGroup.GroupID == currentGroup.GroupID)
                    {
                        this.Dispatcher.BeginInvoke(new Action(() =>
                        {

                            textBoxReceivedMessage.Text += m.ToString();

                        }));
                    }
                }

                foreach (var v in JoinedGroups)
                {
                    comboBoxGroup.Items.Add(v.Key.GroupName);
                }

                this.textBlockGroup.Text = currentGroup.GroupName;

                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    foreach (var v in Messages)
                    {
                        if (v.TargetGroup.GroupName == currentGroup.GroupName)
                            textBoxReceivedMessage.Text += v.UserMessage.ToString();
                    }
                }));
            }));
        }
        string addDateTime()
        {
            return " (@ " + DateTime.Now.ToString("hh:mm:ss") + ")\n\n";

        }

        void MainWindow_OnMessageReceived(Message UserMessage)
        {
            Messages.Add(UserMessage);
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                textBoxReceivedMessage.Text += UserMessage.ToString();
            }));
        }

        void ConnectAsync()
        {


            hubConnection = new HubConnection("http://Tariq-PC:8080/");

            proxy = hubConnection.CreateHubProxy("ChatHub");
            hubConnection.StateChanged += hubConnection_StateChangedAsync;

            try
            {
                if (hubConnection.State != ConnectionState.Connected)
                {
                    hubConnection.Start();


                }
            }
            catch (Exception ex)
            {

            }

        }

        public delegate void MessageReceivedDelegate(Message UserMessage);
        public event MessageReceivedDelegate OnMessageReceived;
        public delegate void StatusMessageReceivedDelegate(Status status, Object[] items);
        public event StatusMessageReceivedDelegate OnStatusMessageReceived;
        void hubConnection_StateChangedAsync(StateChange obj)
        {
            if (obj.NewState == ConnectionState.Connected)
            {
                proxy.Invoke("SignIn", me);

                this.Dispatcher.BeginInvoke(new Action(() =>
                {

                    this.textBoxReceivedMessage.Text += "Connected\n\n";
                    buttonConnect.IsEnabled = false;
                    buttonStop.IsEnabled = true;
                }));

                proxy.On<List<User>>("GetLiveUsers", (users) =>
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        foreach (var v in users)
                        {
                            LiveUsers.Add(v);
                        }
                    }));
                });

                proxy.On<Message>("ReceiveMessage", (m) =>
                {
                    if (OnMessageReceived != null)
                    {
                        OnMessageReceived(m);
                    }
                });

                proxy.On<Status, object[]>("ReceiveStatusMessage", (status, statusItems) =>
                {
                    if (OnStatusMessageReceived != null)
                    {
                        OnStatusMessageReceived(status, statusItems);
                    }
                });

                proxy.On<User, User, byte[]>("ReceivePublicKey", (sender, receiver, publicKey) =>
                {
                    if (EncUser.Username == null)
                    {
                        var v = LiveUsers.FirstOrDefault(x => x.Username == sender.Username);
                        if (v != null)
                        {
                            EncUser = v;
//textBoxCredentials.Text = EncUser.Username;
                            byte[] b = cskg.GeneratePublicKey();
                            this.Dispatcher.BeginInvoke(new Action(() =>
                            {
                                this.textBoxGeneratedKeyPublic.Text = Encoding.UTF8.GetString(b);
                            }));
                            cskg.GenerateSessionKey(publicKey);
                            SessionKey = cskg.ClientKey;
                            this.Dispatcher.BeginInvoke(new Action(() =>
                            {
                                this.textBoxGeneratedKeyPrivate.Text = Encoding.UTF8.GetString(SessionKey);
                            }));
                            this.proxy.Invoke("ExchangePublicKeyCallback", me, EncUser, b);
                        }
                    }
                });

                proxy.On<User, User, byte[]>("ReceivePublicKeyCallback", (sender, receiver, publicKey) =>
                {
                    cskg.GenerateSessionKey(publicKey);
                    SessionKey = cskg.ClientKey;
                    this.Dispatcher.BeginInvoke(new Action(() =>
                   {
                       this.textBoxGeneratedKeyPrivate.Text = Encoding.UTF8.GetString(SessionKey);
                   }));
                });
                proxy.On<User, User, byte[], byte[],byte[]>("ReceiveEncMessage", (sender, receiver, encMessage, iv, hash) =>
                {
                    sct.DecryptAES(SessionKey, encMessage, iv);


                    HashAlgorithm sha512 = new SHA512CryptoServiceProvider();
                    byte[] b = sha512.ComputeHash(Encoding.UTF8.GetBytes(sct.Message));


                   
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {

                        this.textBoxEncMessage.Text += sender.Username+": "+sct.Message+"\n";
                        this.textBoxEncMessage.Text += "senderHash: " + Encoding.UTF8.GetString(hash)+"\n";
                        this.textBoxEncMessage.Text += "receiverHash: " + Encoding.UTF8.GetString(b)+"\n\n";

                    
                    }));
                });
            }


        }

        void SendMessageAsync(User user, UserGroup TargetGroup, string UserMessage)
        {
            this.proxy.Invoke("MulticastMessage", user, TargetGroup, UserMessage);
        }

        private void buttonGenerateKey_Click(object sender, RoutedEventArgs e)
        {
            var v = LiveUsers.FirstOrDefault(x => x.Username == textBoxCredentials.Text);
            if (v != null) EncUser = v;

            byte[] b = cskg.GeneratePublicKey();
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                this.textBoxGeneratedKeyPublic.Text = Encoding.UTF8.GetString(b);
            }));

            this.proxy.Invoke("ExchangePublicKey", me, EncUser, b);
        }

        private void buttonStop_Click(object sender, RoutedEventArgs e)
        {
            if (hubConnection.State == ConnectionState.Connected)
            {
                hubConnection.Stop();
                buttonStop.IsEnabled = false;
                buttonConnect.IsEnabled = true;
            }
        }

        private void buttonConnect_Click(object sender, RoutedEventArgs e)
        {
            me = new User()
            {
                Username = textBoxUsername.Text,
                GroupList = new List<Guid>(),
                UserCredentials = new Credentials()
            };
            this.ConnectAsync();

        }

        private void buttonSelectGroup_Click(object sender, RoutedEventArgs e)
        {
            if (comboBoxGroup.SelectedIndex > -1)
            {
                currentGroup = JoinedGroups.Keys.ToList()[comboBoxGroup.SelectedIndex];
                SetGroupSpecificConfig();
            }
        }

        private void buttonCreateNewGroup_Click(object sender, RoutedEventArgs e)
        {
            this.proxy.Invoke("CreateGroup", new UserGroup() { GroupCreator = me, GroupName = textBoxNewGroup.Text, UserCount = 0 });
        }

        private void textBoxSendMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Message m = new Message();
                m.MessageID = Guid.NewGuid();
                m.Sender = me;
                m.TargetGroup = currentGroup;
                m.Timestamp = DateTime.Now;
                m.UserMessage = textBoxSendMessage.Text;

                textBoxSendMessage.Text = "";
                this.proxy.Invoke("MulticastMessage", m);
            }
        }


        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            bool? b = (sender as CheckBox).IsChecked;
            if (b == true)
            {
                this.proxy.Invoke("AddUserToGroup", currentGroup, LiveUsers.FirstOrDefault(x => x.Username == (sender as CheckBox).Tag.ToString()));
            }
            else if (b == false)
            {
                this.proxy.Invoke("RemoveUserFromGroup", currentGroup, LiveUsers.FirstOrDefault(x => x.Username == (sender as CheckBox).Tag.ToString()));
            }
        }

        private void CheckBox_Loaded(object sender, RoutedEventArgs e)
        {
            checkBoxes.Add(sender as CheckBox);
        }

        private void textBoxSendEncMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                sct.EncryptAES(SessionKey, textBoxSendEncMessage.Text);

                HashAlgorithm sha512 = new SHA512CryptoServiceProvider();
                byte[] b = sha512.ComputeHash(Encoding.UTF8.GetBytes(textBoxSendEncMessage.Text));


                this.proxy.Invoke("EncCommunication", me, EncUser, sct.encryptedMessage, sct.iv,b);
                string str = textBoxSendEncMessage.Text;
                textBoxSendEncMessage.Text = "";
                textBoxEncMessage.Text += me.Username + ": " + str;
            }
        }

    }
}
