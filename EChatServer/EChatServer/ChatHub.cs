﻿using EChatCommon;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EChatServer
{
    public class ChatHub : Hub
    {
        static Dictionary<string, User> LiveUsers { get; set; }
        static Dictionary<Guid, UserGroup> LiveGroups { get; set; }

        public ChatHub()
        {
            if (LiveUsers == null)
                LiveUsers = new Dictionary<string, EChatCommon.User>();
            if (LiveGroups == null)
                LiveGroups = new Dictionary<Guid, EChatCommon.UserGroup>();
        }

        public void SignIn(User user)
        {
            user.UserID = Context.ConnectionId;
            try
            {
                var u = LiveUsers.FirstOrDefault(x => x.Value.Username == user.Username);
                if (u.Value != null) throw new Exception("Duplicate Username");
                LiveUsers.Add(user.UserID, user);
                SendStatusrMessage(Status.SignIn, new User[] { user });
                PullLiveUsers();
            }
            catch (Exception ex)
            {
                SendStatusrMessage(Status.Error, new string[] { ex.Message });
            }
        }

        public void CreateGroup(UserGroup newGroup)
        {
            Guid guid = Guid.NewGuid();
            try
            {
                var g = LiveGroups.FirstOrDefault(x => x.Value.GroupName == newGroup.GroupName && x.Value.GroupCreator.Username == LiveUsers[Context.ConnectionId].Username);
                if (g.Value != null) throw new Exception("Duplicate GroupName");
                else
                {
                    newGroup.GroupID = guid;
                    LiveGroups.Add(guid, newGroup);
                    LiveGroups[guid].UserCount++;
                    LiveUsers[Context.ConnectionId].GroupList.Add(guid);
                    SendStatusrMessage(Status.GroupCreate, new UserGroup[] { LiveGroups[guid] });
                }
            }
            catch (Exception ex)
            {
                SendStatusrMessage(Status.Error, new string[] { ex.Message });
            }
        }

        public void PullLiveUsers()
        {

            List<User> users = LiveUsers.Values.ToList();
            Clients.Client(Context.ConnectionId).GetLiveUsers(users);
        }

        public void AddUserToGroup(UserGroup group, User user)
        {
            try
            {
                var g = LiveGroups.FirstOrDefault(x => x.Value.GroupName == group.GroupName);

                //Check for Existing Group and GroupCreator
                if (g.Value != null && g.Value.GroupCreator.UserID == Context.ConnectionId)
                {
                    var u = LiveUsers.FirstOrDefault(x => x.Value.Username == user.Username);
                    if (u.Value == null) throw new Exception("Username Not Found");

                    //Check if same user isn't added to the same group multiple times
                    else if (!u.Value.GroupList.Exists(y => y == g.Key))
                    {
                        u.Value.GroupList.Add(g.Key);
                        LiveGroups[g.Key].UserCount++;
                        List<User> lu = new List<User>();
                        var usrs = LiveUsers.Where(x => x.Value.GroupList.Exists(y => y == group.GroupID));
                        foreach (var v in usrs)
                        {
                            lu.Add(v.Value);
                        }
                        SendStatusrMessage(Status.GroupJoin, new object[] { LiveUsers[Context.ConnectionId], user, group, lu });

                    }
                    else
                    {
                        throw new Exception("User Already Added");
                    }

                }
                else
                {
                    throw new Exception("Group Not Found");
                }
            }
            catch (Exception ex)
            {
                SendStatusrMessage(Status.Error, new string[] { ex.Message });
            }
        }

        public void RemoveUserFromGroup(UserGroup group, User user)
        {
            try
            {
                var g = LiveGroups.FirstOrDefault(x => x.Value.GroupName == group.GroupName);
                var u = LiveUsers.FirstOrDefault(x => x.Value.Username == user.Username);
                if (g.Value == null) throw new Exception("Group Not Found");
                else if (u.Value == null) throw new Exception("User Not Found");
                {

                    SendStatusrMessage(Status.GroupLeave, new object[] { LiveUsers[Context.ConnectionId], user, group });
                    Guid guid = LiveGroups.FirstOrDefault(x => x.Value.GroupName == group.GroupName).Key;
                    u.Value.GroupList.Remove(guid);
                    LiveGroups[guid].UserCount--;
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void MulticastMessage(Message message)
        {
            try
            {
                var g = LiveGroups.FirstOrDefault(x => x.Value.GroupName == message.TargetGroup.GroupName);
                if (g.Value == null) throw new Exception("Group Not Found");
                else
                {
                    //Check the lists of users added to group g.
                    foreach (var v in LiveUsers.Where(x => x.Value.GroupList.Exists(y => y == g.Key)))
                    {
                        Clients.Client(v.Key).ReceiveMessage(message);
                    }
                }


            }
            catch (Exception ex)
            {
                SendStatusrMessage(Status.Error, new string[] { ex.Message });
            }
        }

        public void EncCommunication(User Sender, User Receiver, byte[] EncMessage, byte[] iv,byte[] hash)
        {
            Console.WriteLine(Encoding.UTF8.GetString(EncMessage));
            EncMessage[EncMessage.Length - 2] = 2;
            Clients.Client(Receiver.UserID).ReceiveEncMessage(Sender,Receiver,EncMessage,iv,hash);
        }
        public void ExchangePublicKey(User Sender, User Receiver, byte[] PublicKey)
        {
            string str = LiveUsers.FirstOrDefault(x => x.Value.Username == Receiver.Username).Key;
            Clients.Client(str).ReceivePublicKey(Sender, Receiver, PublicKey);
        }

        public void ExchangePublicKeyCallback(User Sender, User Receiver, byte[] PublicKey)
        {
            string str = LiveUsers.FirstOrDefault(x => x.Value.Username == Receiver.Username).Key;
            Clients.Client(str).ReceivePublicKeyCallback(Sender, Receiver, PublicKey);
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            try
            {

                var v = LiveUsers[Context.ConnectionId];
                LiveUsers.Remove(Context.ConnectionId);

                foreach (var g in v.GroupList)
                {
                    LiveGroups[g].UserCount--;


                }
                SendStatusrMessage(Status.SignOut, new User[] { v });

                var keys = LiveGroups.Where(x => x.Value.UserCount == 0);
                foreach (var k in keys)
                {
                    LiveGroups.Remove(k.Key);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return base.OnDisconnected(stopCalled);
        }

        void SendStatusrMessage(Status status, object[] StatusObject)
        {
            if (status == Status.SignIn || status == Status.SignOut)
            {
                foreach (var v in LiveUsers)
                    Clients.Client(v.Key).ReceiveStatusMessage(status, StatusObject);
            }

            else if (status == Status.Error || status == Status.GroupCreate)
            {
                Clients.Client(Context.ConnectionId).ReceiveStatusMessage(status, StatusObject);
            }
            else if (status == Status.GroupJoin || status == Status.GroupLeave)
            {
                Guid guid = ((UserGroup)StatusObject[2]).GroupID;

                foreach (var u in LiveUsers.Values)
                {
                    if (u.GroupList.Exists(x => x == guid))
                    {
                        Clients.Client(u.UserID).ReceiveStatusMessage(status, StatusObject);
                    }
                }

            }
        }
    }
}
