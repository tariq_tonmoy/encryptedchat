﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace EChatServer
{
    class Program
    {
        static void Main(string[] args)
        {
            string localhost = Dns.GetHostEntry(Dns.GetHostName()).AddressList.First(x => x.AddressFamily == AddressFamily.InterNetwork).ToString();
            string url = "http://" + localhost + ":8080";
            using (WebApp.Start(url))
            {
                Console.WriteLine("Server running on " + url + Environment.NewLine);
                Console.ReadLine();
            }

            Console.WriteLine("done");

            Console.ReadLine();
        }
    }
}
