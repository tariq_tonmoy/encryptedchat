﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EChatCommon
{
    public class User
    {
        public string UserID { get; set; }
        public string Username { get; set; }
        public List<Guid> GroupList { get; set; }
        public Credentials UserCredentials { get; set; }

        public override string ToString()
        {

            return this.Username;
        }
    }

}
