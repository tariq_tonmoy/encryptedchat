﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EChatCommon
{
    public class UserGroup
    {
        public Guid GroupID { get; set; }
        public string GroupName { get; set; }
        public User GroupCreator { get; set; }

        public Credentials GroupCredentials { get; set; }
        public int UserCount { get; set; }
    }
}
