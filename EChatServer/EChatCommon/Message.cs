﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EChatCommon
{
    public class Message
    {
        public Guid MessageID { get; set; }
        public User Sender { get; set; }
        public UserGroup TargetGroup { get; set; }
        public String UserMessage { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
