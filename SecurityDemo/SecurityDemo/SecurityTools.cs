﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SecurityDemo
{
    public class SecurityTools
    {
        RSACryptoServiceProvider RSA_Priv = new RSACryptoServiceProvider();
        public RSAParameters RSA_Public_Key()
        {


            RSAParameters KeyInfo = RSA_Priv.ExportParameters(false);

            return KeyInfo;

        }

        public byte[] Encrypt_Msg(string message, RSAParameters KeyInfo)
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            RSA.ImportParameters(KeyInfo);

            return RSA.Encrypt(Encoding.UTF8.GetBytes(message), false);
        }

        public string Decrypt_Msg(byte[] enc_message)
        {
            byte[] dec = RSA_Priv.Decrypt(enc_message, false);
            return Encoding.UTF8.GetString(dec);
        }

        public string GetHMACMD5(string message)
        {
            HMACMD5 hmacMd5 = new HMACMD5();
            byte[] key = Guid.NewGuid().ToByteArray();

            hmacMd5.Key = key;
            byte[] b= hmacMd5.ComputeHash(Encoding.UTF8.GetBytes(message));

            return Encoding.UTF8.GetString(b);


        }

        public string GetHMACMD5(string message,byte[] key)
        {
            HMACMD5 hmacMd5 = new HMACMD5();
            hmacMd5.Key = key;
            byte[] b = hmacMd5.ComputeHash(Encoding.UTF8.GetBytes(message));

            return Encoding.UTF8.GetString(b);


        }

        public String GetSHA512(string message)
        {
            HashAlgorithm sha512 = new SHA1CryptoServiceProvider();
            byte[] b = sha512.ComputeHash(Encoding.UTF8.GetBytes(message));

            return Encoding.UTF8.GetString(b);
        }


    }
}
