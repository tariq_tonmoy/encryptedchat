﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SecurityDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            SecurityTools securityTools = new SecurityTools();
            RSAParameters KeyInfo=securityTools.RSA_Public_Key();
            byte[] b = securityTools.Encrypt_Msg("This is a Text",KeyInfo);

            Console.WriteLine(Encoding.UTF8.GetString(b));

            string str = securityTools.Decrypt_Msg(b);
            Console.WriteLine(str);

            Console.ReadLine();

            for (int i = 0; i < 5; i++)
            {
                string md5Hash = securityTools.GetHMACMD5(str);
                Console.WriteLine(md5Hash);
            }
            Console.ReadLine();
            byte[] key=Guid.NewGuid().ToByteArray();
            for (int i = 0; i < 5; i++)
            {
                string md5Hash = securityTools.GetHMACMD5(str,key);
                Console.WriteLine(md5Hash);
            }

            Console.ReadLine();

            key = Guid.NewGuid().ToByteArray();
            for (int i = 0; i < 5; i++)
            {
                string md5Hash = securityTools.GetHMACMD5(str+i.ToString(), key);
                Console.WriteLine(md5Hash);
            }
            Console.ReadLine();

            string SHA512 = securityTools.GetSHA512(str);
            Console.WriteLine(SHA512);
            Console.WriteLine("Length: " + SHA512.Length.ToString());

            Console.ReadLine();

        }
    }
}
