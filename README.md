# Secured Chat Software using Encryption #
* Client-Server architecture using **SignalR**
* Asymmetric Encryption using **RSA**
* Symmetric encryption using **AES**
* Key Exchange for **AES** using **Elliptic Curve Cryptography Diffie Hellman Key Exchange** 
* Hash fucntion demo: **HMACMD**, **SHA512**